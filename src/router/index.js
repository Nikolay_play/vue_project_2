import Vue from 'vue'
import VueRouter from 'vue-router'
import Rubrics from '../components/Rubrics.vue'

Vue.use(VueRouter)

const routes = [{
  path: '/',
  name: 'Rubrics',
  component: Rubrics
}]

const router = new VueRouter({
  routes
})

export default router
